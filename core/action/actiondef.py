class BaseAction:
    def __init__(self):
        self.initiator = None

    def bind(self, initiator):
        assert self.initiator is None
        self.initiator = initiator

    def isbinded(self):
        return self.initiator is not None

    def isactual(self, gamestate, turnmachine):
        return True

    def perform(self, gamestate, turnmachine):
        return self.onperform(gamestate, turnmachine)

    def onperform(self, gamestate, turnmachine):
        raise NotImplementedError("need override this method in subclass")

    def alternate(self, action, delay = 0):
        action.bind(self.initiator, self.gamestate)
        return ActionResult.delay(delay, action)

class ActionResult:
    def __init__(self, success, alternative, cooldown, delay):
        self.success = success
        self.alternative = alternative
        self.cooldown = cooldown
        self.delay = delay
        pass

    def alternate(action):
        return ActionResult(True, action, 0, 0)

    def fail():
        return ActionResult(False, None, 0, 0)

    def success(cooldown = 0):
        return ActionResult(True, None, cooldown, 0)

    def delay(delay, action):
        return ActionResult(True, action, 0, delay)
