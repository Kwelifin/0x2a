from core.action.actiondef import BaseAction, ActionResult

class CloseAction(BaseAction):
    def onperform(self, gamestate, turnmachine):
        turnmachine.poweroff()
        return ActionResult.success()
