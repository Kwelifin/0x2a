from core.action.actiondef import BaseAction, ActionResult

import option

class MoveAction(BaseAction):
    def __init__(self, dirx, diry):
        super().__init__()
        self.dirx = dirx
        self.diry = diry

    def isactual(self, gamestate, turnmachine):
        return self.initiator in gamestate.get_characters()

    def onperform(self, gamestate, turnmachine):
        if not self.isactual(gamestate, turnmachine):
            return ActionResult.fail()

        initiator = self.initiator

        target_x, target_y = initiator.get_position()
        target_x += self.dirx
        target_y += self.diry

        map = initiator.get_map()
        tiledef = map.get_tile(target_x, target_y)

        if tiledef.blocked:
            return ActionResult.fail()

        initiator.set_position((target_x, target_y))

        return ActionResult.success(option.TURN_DURATION)
