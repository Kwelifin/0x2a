from core.action.actiondef import BaseAction, ActionResult

class WaitAction(BaseAction):
    def __init__(self, duration):
        super().__init__()
        self.duration = duration

    def isactual(self, gamestate, turnmachine):
        return self.initiator in gamestate.get_characters()

    def onperform(self, gamestate, turnmachine):
        return ActionResult.success(self.duration)
