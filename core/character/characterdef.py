import utils.math as math
import utils.pattern as pattern

import utils.random as random

import utils.rasterization as rasterization

class Character:
    def __init__(self, controller):
        self.controller = controller
        self.stat = CharacterStats()
        self.map = None
        self.vision_memory = dict()

    def set_map(self, map, newpos):
        if self.map is not None:
            self.map.remove_character(self)
        self.map = map

        if map is None:
            return

        if map not in self.vision_memory:
            self.vision_memory[map] = list()

        self.map.add_character(self, newpos)

    def get_map(self):
        return self.map

    def set_position(self, position):
        self.map.move_character(self, position)

    def get_position(self):
        return self.map.get_character_position(self)

    def get_vision(self):
        current_x, current_y = self.get_position()
        vision_radius = 10

        vision = rasterization.circle(current_x, current_y, vision_radius)
        for pos in vision:
            if pos not in self.vision_memory[self.get_map()]:
                self.vision_memory[self.get_map()].append(pos)

        return vision

    def get_vision_memory(self, map = None):
        if map is None:
            map = self.get_map()
        return self.vision_memory[map]


class CharacterStats:
    def __init__(self, speed = 6):
        self.cooldown = Stat("cooldown", 0, minlimit = 0)
        self.speed = Stat("speed", speed)

class Stat:
    def __init__(self, name, initvalue, minlimit = -math.inf, maxlimit = math.inf):
        self._assert_limit(minlimit)
        self._assert_limit(maxlimit)

        self.name = name
        self.eventhandler = pattern.EventHandler()
        self._minlimit = minlimit
        self._maxlimit = maxlimit

        if type(minlimit) is Stat:
            minlimit.attach(self.onchange_limit)

        if type(maxlimit) is Stat:
            maxlimit.attach(self.onchange_limit)

        self._value = math.clip(initvalue, self._get_limit_value(minlimit), self._get_limit_value(maxlimit))

    def _assert_limit(self, limitation):
        if(__debug__):
            limittype = type(limitation)
            validtypes = [int, Stat, float]
            assert limittype in validtypes

    def onchange_limit(self, delta):
        self.set(self._value)

    def attach(self, onchange):
        self.eventhandler.attach(onchange)

    def deattach(self, onchange):
        self.eventhandler.deattach(onchange)

    def _get_limit_value(self, limit):
        if type(limit) is Stat:
            return limit.get()
        if type(limit) in [int, float]:
            return limit
        raise NotImplementedError("not supported limit type")

    def get(self):
        return self._value

    def set(self, newvalue):
        newvalue = math.clip(newvalue, self._get_limit_value(self._minlimit), self._get_limit_value(self._maxlimit))
        oldvalue = self._value
        self._value = newvalue
        self.eventhandler.invoke(newvalue - oldvalue)

    def dec(self, value = 1):
        self.set(self.get() - value)

    def inc(self, value = 1):
        self.set(self.get() + value)

