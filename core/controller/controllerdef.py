import abc

class BaseController:
    __metaclass__ = abc.ABCMeta

    def need_render(self):
        return False

    def need_input(self):
        return False

    @abc.abstractmethod
    def take_turn(self, character, gamestate, turnmachine):
        raise NotImplementedError("need override method in subclass")
