from core.controller.controllerdef import BaseController
class PlayerController(BaseController):
    def __init__(self):
        self.cmd = None

    def need_render(self):
        return True

    def need_input(self):
        return self.cmd is None

    def take_turn(self, character, gamestate, turnmachine):
        # check if need input for take turn
        if self.need_input():
            return

        current_action = self.cmd
        current_action.bind(character) # assotiate action with current character
        self.cmd = None

        return current_action
