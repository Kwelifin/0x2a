from core.controller.player import PlayerController
from core.map.mapdef import MapGenerator

class ControllerCollection:
    def __init__(self):
        self.player = PlayerController()

class GameState:
    """Represent all game data"""
    def __init__(self):
        self.controllers = ControllerCollection()
        self.characters = list()
        self.current_map = None

    def get_characters(self):
        return self.characters

    def set_map(self, map):
        self.current_map = map

    def get_map(self):
        return self.current_map
