from utils.rasterization import line
from core.shape.rect import Rect

class Tile:
    def __init__(self, char, blocked):
        self.char = char
        self.blocked = blocked

class TileDef:
    VOID = Tile(char=" ", blocked=True)
    FLOOR = Tile(char=".", blocked=False)

class Map:
    def __init__(self, size, layer_tile):
        self.layer_tile = layer_tile
        self.size = size
        self.characters = dict()

    # CHARACTERS #
    def add_character(self, character, position):
        assert character not in self.characters.keys()
        assert position not in self.characters.keys()

        self.characters[character] = position
        self.characters[position] = character

    def remove_character(self, character):
        assert character in self.characters.keys()
        charpos = self.get_character_position(character)

        del self.characters[character]
        del self.characters[charpos]

    def get_character(self, position):
        if position in self.characters:
            return self.characters[position]
        return None

    def get_character_position(self, character):
        assert character in self.characters.keys()
        return self.characters[character]

    def move_character(self, character, position):
        self.remove_character(character)
        self.add_character(character, position)

    # TILES #
    def get_tile(self, x, y):
        width, height = self.get_size()
        if not (width > x >= 0) or not (height > y >= 0):
            return TileDef.VOID
        return self.layer_tile.get_tile(x, y)

    def get_size(self):
        return self.size


class TileLayer:
    def __init__(self, size, tiles):
        self.tiles = tiles
        self.size = size

    def get_tile(self, x, y):
        assert 0 <= x < self.width()
        assert 0 <= y < self.height()
        return self.tiles[y*self.width() + x]

    def width(self):
        return self.size[0]

    def height(self):
        return self.size[1]

    def set_tile(self, x, y, tiletype):
        assert 0 <= x < self.size.width
        assert 0 <= y < self.size.height
        self.tiles[y*self.width()+x] = tiletype


class MapBuilder:
    def __init__(self):
        width, height = 5, 5
        self.size = (width, height)
        self.tiles = [TileDef.VOID]*(width*height)

    def _resize_map(self, neww, newh):
        assert neww > 1 and newh > 1
        resized = [TileDef.VOID]*(neww * newh)

        # Copy old TileTypes
        for y in range(min(self.height(), newh)):
            for x in range(min(self.width(), neww)):
                tiletype = self._get_tile(x, y)
                resized[y*neww+x] = tiletype

        self.tiles = resized
        self.size = (neww, newh)

    def width(self):
        return self.size[0]

    def height(self):
        return self.size[1]

    def _get_tile(self, x, y):
        assert 0 <= x < self.width()
        assert 0 <= y < self.height()
        return self.tiles[y*self.width() + x]

    def _set_tile(self, x, y, tiletype):
        assert 0 <= x < self.width()
        assert 0 <= y < self.height()
        self.tiles[y*self.width()+x] = tiletype

    def set_size(self, width, height):
        self._resize_map(width, height)

    def fill(self, tiletype, rect = None):
        if rect is None:
            rect = Rect((0, 0), self.size)

        for y in range(rect.y(), rect.y() + rect.height()):
            for x in range(rect.x(), rect.x() + rect.width()):
                self._set_tile(x, y, tiletype)

    def line(self, tiletype, source, dest):
        x0, y0 = source
        x1, y1 = dest
        for x, y in line(x0, y0, x1, y1):
            self._set_tile(x, y, tiletype)

    def build(self):
        copy_tiles = self.tiles.copy()
        copy_size = self.size

        layer_tile = TileLayer(copy_size, copy_tiles)
        return Map(copy_size, layer_tile)

    def _debug_drawbuilder(self):
        for y in range(self.size.height):
            for x in range(self.size.width):
                tile = self._get_tile(x, y)
                print(tile.char, end="")
            print()

class MapGenerator:
    def __init__(self):
        pass

    def generate(self):
        mapbuilder = MapBuilder()
        mapbuilder.set_size(40, 30)

        # 1 room
        mapbuilder.fill(TileDef.FLOOR, Rect((4, 10), (6, 8)))
        mapbuilder.fill(TileDef.FLOOR, Rect((8, 11), (6, 5)))

        # 2 room
        mapbuilder.fill(TileDef.FLOOR, Rect((16, 15), (10, 9)))

        # 3 room
        mapbuilder.fill(TileDef.FLOOR, Rect((20, 4), (7, 9)))

        # 4 room
        mapbuilder.fill(TileDef.FLOOR, Rect((30, 22), (8, 6)))

        # 1-2
        mapbuilder.line(TileDef.FLOOR, (8, 16), (20, 16))
        # 1-3
        mapbuilder.line(TileDef.FLOOR, (5, 11), (22, 6))
        # 2-4
        mapbuilder.line(TileDef.FLOOR, (20, 20), (34, 25))
        return mapbuilder.build()
