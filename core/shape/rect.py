from core.shape.shapedef import Shape

class Rect(Shape):
    def __init__(self, position, size):
        self.position = position
        self.size = size

    def x(self):
        return self.position[0]

    def y(self):
        return self.position[1]

    def width(self):
        return self.size[0]

    def height(self):
        return self.size[1]

    def get_positions(self):
        positions = []
        for x in range(self.x(), self.x() + self.width()):
            for y in range(self.y(), self.y() + self.height()):
                positions.append((x, y))
        return positions

    def intersect(self, shape):
        if isinstance(shape, Rect):
            # optimized algorithm with rect
            return not (self.x() > shape.x() + shape.width() or
                       self.x() + self.width() < shape.x() or
                       self.y() > shape.y() + shape.height() or
                       self.y() + self.height() < shape.y());
        return super().intersect(shape)
