class Shape:
    """Base class for standart shapes"""
    def get_positions(self):
        raise NotImplementedError("need override in subclass")

    def intersect(self, shape):
        selfpos = self.get_positions()
        otherpos = shape.get_positions()

        for pos in selfpos:
            if pos in otherpos:
                return True
        return False