import utils.random as random

class TurnMachine:
    def __init__(self, gamestate, turnplan = None, turncounter = 0):
        self.gamestate = gamestate
        self.enabled = False
        self.turncounter = turncounter
        self.turnplan = dict() if turnplan is None else turnplan

    def loadstate(self, state):
        # TODO need restore from savestring
        pass

    def storestate(self):
        # TODO need return savestring
        pass

    def poweroff(self):
        self.enabled = False

    def perform(self, action):
        action_result = action.perform(self.gamestate, self)

        # check zero delayed alternative action
        while action_result.alternative is not None and action_result.delay == 0:
            action = action_result.alternate
            action_result = action.perform(self.gamestate, self)

        # check delayed alternative action
        if action_result.alternative is not None and action_result.delay > 0:
            self.schedule_action(self.turncounter + action_result.delay, action_result.alternate)

        return action_result

    def turnloop(self):
        self.enabled = True

        # render screen and check user input before start loop
        yield TurnResult(need_render = True, need_input = True)

        while self.enabled:
            # on turn start perform all scheduled actions
            self.perform_planned(self.turncounter)

            characters = self.gamestate.get_characters()
            # TODO order characters by initiative
            for character in characters:
                # perform character actions while it not in cooldown
                while character.stat.cooldown.get() <= 0:
                    controller = character.controller
                    if controller.need_render() or controller.need_input():
                        yield TurnResult(controller.need_render(), controller.need_input(), controller, character)

                    # check poweroff after user input
                    if not self.enabled:
                        return

                    action = controller.take_turn(character, self.gamestate, self)

                    if action is None:
                        continue

                    action_result = self.perform(action)
                    if action_result.success:
                        character.stat.cooldown.set(action_result.cooldown)

                # decrease cooldown
                character.stat.cooldown.dec()

            # turn completed go to next
            self.turncounter += 1
            yield TurnResult(need_render = False, need_input = True)

    def perform_planned(self, turn):
        while turn in self.turnplan.keys():
            act = self.take_action(turn)

            if act.isactual():
                self.perform(act)

    def schedule_action(self, turn, action):
        assert turn > self.turncounter

        if turn in self.turnplan.keys():
            self.turnplan[turn].append(action)
            return

        actionlist = list()
        actionlist.append(action)
        self.turncounter[turn] = actionlist

    def take_action(self, turn):
        actions = self.turnplan[turn]
        actindex = random.randint(0, len(actions)-1)
        act = actions[actindex]
        del actions[actindex]
        if len(actions) == 0:
            del self.turnplan[turn]
        return act

class TurnResult:
    def __init__(self, need_render, need_input, controller = None, character = None):
        self.need_render = need_render
        self.need_input = need_input
        self.controller = controller
        self.character = character
