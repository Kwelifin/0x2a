from enum import Enum
from bearlibterminal import terminal
from copy import copy

class InputDomain(Enum):
    GLOBAL = 0
    BASE = 1

class BindingDef:
    def __init__(self, sys, command, key, ctrl, alt, shift):
        self.sys = sys
        self.cmd = command
        self.key = key
        self.ctrl = ctrl
        self.alt = alt
        self.shift = shift

    def check(self, input):
        if self.key != input:
            return False

        return (terminal.check(terminal.TK_CONTROL) == self.ctrl
                and terminal.check(terminal.TK_ALT) == self.alt
                and terminal.check(terminal.TK_SHIFT) == self.shift)


class InputHandler:
    def __init__(self):
        self.binds = dict()
        self.domain = InputDomain.BASE

    def bind(self, domain, commanddef):
        if domain not in self.binds:
            self.binds[domain] = list()
        self.binds[domain].append(commanddef)

    def handle(self, input, controller, gamestate, turnmachine):
        binding = self._search_binding(self.domain, input) # search in local domain
        if binding is None: # local binding not found?
            binding = self._search_binding(InputDomain.GLOBAL, input) # search in global domain

        if binding is None: 
            return # binding not found

        cmd = copy(binding.cmd)

        if binding.sys: # check system binding
            cmd.perform(gamestate, turnmachine) # perform command immeditly
            return

        if controller is None:
            return

        controller.cmd = cmd # set next controller command

    def _search_binding(self, domain, input):
        if domain not in self.binds:
            return
        for binding in self.binds[domain]:
            if binding.check(input):
                return binding

    def switch_domain(self, newdomain):
        assert newdomain != InputDomain.GLOBAL
        self.domain = newdomain