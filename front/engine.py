from bearlibterminal import terminal

from front.frame import Frame

"""
LOGO
▄ •▄       ▄▄▄▄·       ▄▄▌  ·▄▄▄▄      • ▌ ▄ ·.  ▄▄▄·  ▄▄ • ▪   ▄▄· 
█▌▄▌▪▪     ▐█ ▀█▪▪     ██•  ██▪ ██     ·██ ▐███▪▐█ ▀█ ▐█ ▀ ▪██ ▐█ ▌▪
▐█▀▄· ▄█▀▄ ▐█▀▀█▄ ▄█▀▄ ██▪  ▐█· ▐█▌    ▐█ ▌▐▌▐█·▄█▀▀█ ▄█ ▀█▄▐█·██ ▄▄
▐█.█▌▐█▌.▐▌██▄▪▐█▐█▌.▐▌▐█▌▐▌██. ██     ██ ██▌▐█▌▐█ ▪▐▌▐█▄▪▐█▐█▌▐███▌
·▀  ▀ ▀█▄▀▪·▀▀▀▀  ▀█▄▀▪.▀▀▀ ▀▀▀▀▀•     ▀▀  █▪▀▀▀ ▀  ▀ ·▀▀▀▀ ▀▀▀·▀▀▀  
"""

class GameEngine:
    def __init__(self, gameinit, inputinit):
        # terminal.set("""
        #     window.title='foo'; window.size=80x25;
        # """)
        self.turnmachine, self.gamestate = gameinit.init()
        self.inputhandler = inputinit.init()

    def _render_cell(self, frame, map, x, y, isvisible):
        if isvisible:
            character = map.get_character((x, y))
            if character is not None:
                frame.put(x, y, "@")
                return

        tiledef = map.get_tile(x, y)
        if isvisible:
            frame.put(x, y, tiledef.char)
        else:
            frame.put(x, y, tiledef.char, terminal.color_from_argb(255, 100, 100, 100))

    def _render_character_vision(self, character):
        assert character is not None

        gamemap = character.get_map()
        map_width, map_height = gamemap.get_size()

        char_x, char_y = character.get_position()

        vision_cells = character.get_vision()
        vision_memory_cells = character.get_vision_memory()

        board_frame = Frame(map_width, map_height)
        board_frame.centerby(char_x, char_y)

        for x, y in vision_memory_cells:
            self._render_cell(board_frame, gamemap, x, y, (x, y) in vision_cells)
        # for x, y in vision_cells:
        #     tiledef = gamemap.get_tile(x, y)
        #     board_frame.put(x, y, tiledef.char)


    def render(self, character):
        turnmachine = self.turnmachine
        gamestate = self.gamestate

        terminal.clear()

        if character is not None:
            self._render_character_vision(character)

        terminal.printf(0, 0, f"turn:{self.turnmachine.turncounter}")

        terminal.refresh()

    def handle_input(self, controller):
        if not terminal.has_input():
            return
        key = terminal.read()
        self.inputhandler.handle(key, controller, self.gamestate, self.turnmachine)

    def run(self):
        terminal.open()
        terminal.set("""
            window:
                title='Kobold Magic (0x2A)'
        """)

        for turn_result in self.turnmachine.turnloop():
            if turn_result.need_render:
                self.render(turn_result.character)

            if turn_result.need_input:
                self.handle_input(turn_result.controller)

        terminal.close()
