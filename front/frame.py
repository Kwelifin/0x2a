from bearlibterminal import terminal

class Frame:
    def __init__(self, width = None, height = None):
        if width is None:
            width = terminal.state(terminal.TK_WIDTH)
        if height is None:
            height = terminal.state(terminal.TK_HEIGHT)

        self.width = width
        self.height = height
        self.center = (width//2, height//2)
        self.x = 0
        self.y = 0

    def terminal_center(self):
        terminal_width, terminal_height = terminal.state(terminal.TK_WIDTH), terminal.state(terminal.TK_HEIGHT) 
        center_pos = (terminal_width//2, terminal_height//2)
        return center_pos

    def move(self, x, y):
        self.x = x
        self.y = y

    def centerby(self, center_x, center_y):
        self.center = (center_x, center_y)

    def put(self, x, y, char, color = None):
        if not (self.width > x >= 0) or not (self.height > y >= 0):
            return

        # if color is not None:
        #     oldcolor = terminal.state(terminal.TK_COLOR)
        #     terminal.color(color)

        if color is None:
            color = terminal.color_from_argb(255, 255, 255, 255)

        offset_x , offset_y = self.x, self.y

        center_x, center_y = self.center
        terminal_center_x, terminal_center_y = self.terminal_center()

        relative_x = offset_x + x + terminal_center_x - center_x
        relative_y = offset_y + y + terminal_center_y - center_y
        terminal.put_ext(relative_x, relative_y, 0, 0, char, [color]*4)

        # if color is not None:
        #     terminal.color(oldcolor)
