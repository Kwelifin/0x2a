from core.gamestate import GameState
from core.turnmachine import TurnMachine

from core.character.characterdef import Character

from core.map.mapdef import MapGenerator

class GameInitializer:
    def __init__(self):
        pass

    def init(self):
        # initialize gamestate
        gamestate = GameState()

        map = MapGenerator().generate()
        player_char = Character(gamestate.controllers.player)
        player_char.set_map(map, (11, 12))

        # player_char2 = Character(gamestate.controllers.player)
        # player_char2.set_map(map, (13, 12))

        gamestate.set_map(map)
        gamestate.characters.append(player_char)
        # gamestate.characters.append(player_char2)

        # initialize turnmachine
        machine = TurnMachine(gamestate)

        return (machine, gamestate)
