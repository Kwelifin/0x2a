from bearlibterminal import terminal

from front.control import InputDomain, BindingDef, InputHandler

import option

from core.action.close import CloseAction
from core.action.wait import WaitAction
from core.action.move import MoveAction

class InputInitializer:
    def __init__(self):
        pass

    def init(self):
        inputhandler = InputHandler()
        #                 Domain                         Sys    Command                           Key                  Ctrl   Alt    Shift
        # SYSTEM
        inputhandler.bind(InputDomain.GLOBAL, BindingDef(True,  CloseAction(),                    terminal.TK_ESCAPE, False, False, False))
        inputhandler.bind(InputDomain.GLOBAL, BindingDef(True,  CloseAction(),                    terminal.TK_CLOSE,  False, False, False))

        # MOVE
        inputhandler.bind(InputDomain.BASE,   BindingDef(False, MoveAction(0, 1),                 terminal.TK_KP_2,   False, False, False))
        inputhandler.bind(InputDomain.BASE,   BindingDef(False, MoveAction(-1, 0),                 terminal.TK_KP_4,   False, False, False))
        inputhandler.bind(InputDomain.BASE,   BindingDef(False, MoveAction(1, 0),                 terminal.TK_KP_6,   False, False, False))
        inputhandler.bind(InputDomain.BASE,   BindingDef(False, MoveAction(0, -1),                 terminal.TK_KP_8,   False, False, False))
        inputhandler.bind(InputDomain.BASE,   BindingDef(False, MoveAction(-1, 1),                 terminal.TK_KP_1,   False, False, False))
        inputhandler.bind(InputDomain.BASE,   BindingDef(False, MoveAction(-1, -1),                 terminal.TK_KP_7,   False, False, False))
        inputhandler.bind(InputDomain.BASE,   BindingDef(False, MoveAction(1, -1),                 terminal.TK_KP_9,   False, False, False))
        inputhandler.bind(InputDomain.BASE,   BindingDef(False, MoveAction(1, 1),                 terminal.TK_KP_3,   False, False, False))
        inputhandler.bind(InputDomain.BASE,   BindingDef(False, WaitAction(option.TURN_DURATION), terminal.TK_KP_5,   False, False, False))
        return inputhandler
