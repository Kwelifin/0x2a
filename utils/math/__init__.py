import math

# infinity
inf = math.inf
infinity = inf

# functions
def clip(value, minval, maxval):
    return max(min(value, maxval), minval)