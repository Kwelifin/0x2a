class EventHandler:
    def __init__(self):
        self.listeners = list()

    def attach(self, func):
        self.listeners.append(func)

    def deattach(self, func):
        self.listeners.remove(func)

    def invoke(self, *args):
        for func in self.listeners:
            func(*args)