import random

def seed(seed = None):
    random.seed(seed)

def choice(collection):
    return random.choice(collection)

def rand(min, max):
    return random.rand(min, max)

def randint(min, max):
    return random.randint(min, max)