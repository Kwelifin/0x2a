def line(x0, y0, x1, y1):
    steep = abs(y1 - y0) > abs(x1 - x0)
    if steep:
        x0, y0 = y0, x0
        x1, y1 = y1, x1

    if x0 > x1:
        x0, x1 = x1, x0
        y0, y1 = y1, y0

    deltax = abs(x1 - x0)
    deltay = abs(y1 - y0)
    error = deltax // 2
    ystep = 1 if y0 < y1 else -1
    y = y0

    positions = list()
    for x in range(x0, x1+1):
        newpos = (y, x) if steep else (x, y)
        positions.append(newpos)

        error -= deltay
        if error < 0:
            y += ystep
            error += deltax
    return positions

def circle(cx, cy, radius):
    d = 3 - 2 * radius;

    x, y = radius, 0
    positions = []
    while y <= x:
        positions.extend(line(cx - x, cy - y, cx + x, cy - y))
        positions.extend(line(cx - x, cy + y, cx + x, cy + y))
        positions.extend(line(cx - y, cy + x, cx + y, cy + x))
        positions.extend(line(cx - y, cy - x, cx + y, cy - x))

        if d <= 0:
            d += 4 * y + 6
            y += 1
        else:
            d += 4 * (y - x) + 10
            x -= 1
            y += 1

    return positions
