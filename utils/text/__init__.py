def is_empty_or_whitespace(line):
    return not line or line.isspace()

def check_unwrapable_word(word, max_line_width):
    return len(word) >= max_line_width

def wrap_long_word(word, current_position, max_line_width):
    wrapped_lines = list()
    delimiter = "-"
    while len(word) > max_line_width:
        split_position = max_line_width-current_position-len(delimiter)
        word_part = word[:split_position]
        word = word[split_position:]
        wrapped_lines.append(word_part + delimiter)
        current_position = 0
    if len(word) > 0:
        wrapped_lines.append(word)
    return wrapped_lines


def wrap_by_words(line, max_line_width):
    wrapped_lines = list()

    if not line or line.isspace():
        return wrapped_lines

    if len(line) <= max_line_width:
        wrapped_lines.append(line)
    else:
        expected_line = ""
        for word in line.split(" "):
            extend = word + " "
            extended_expected_line = expected_line + extend
            if len(extended_expected_line) >= max_line_width:
                if check_unwrapable_word(extend, max_line_width):
                    wrapped_word_lines = wrap_long_word(extend, len(expected_line), max_line_width)
                    current_line_extend_part = wrapped_word_lines[0]
                    wrapped_lines.append(expected_line + current_line_extend_part)
                    wrapped_lines.extend(wrapped_word_lines[1:-1])
                    expected_line = wrapped_word_lines[-1]
                    continue
                if not is_empty_or_whitespace(expected_line):
                    wrapped_lines.append(expected_line)
                expected_line = extend
            else:
                expected_line = extended_expected_line
        if not expected_line.isspace():
            wrapped_lines.append(expected_line)

    return wrapped_lines
